# Python FYP module

Python module to analyze body language with data coming from Kinect

## Installation

You must have python3 and pip installed (pip comes with python3 installation)

Create a virtual environment (venv):

### Windows

Creation:

`c:\>python -m venv c:\path\to\myenv`

Activation:

`'.\venv\Scripts\activate'`

You shoud see now your path with (venv) at the beginning.

Now you can install the dependencies:

`pip install -r requirements.txt`

## How to use

Once you installed the dependencies, you have to first run the FYP-Multiple project and
then run "Process realtime - plot.py" with python:

`python '.\Process realtime - plot.py' `

Now you can move in front of the Kinect, the python code will work in background and
add data in plotting.txt.


Once you are done, stop the script and launch the "Process and plot.py" file. It will show you the 
data collected in a plot.