# FYP-Multiple

Welcome to FYP-Multiple, the body language analyzer made by Fouand Hannoun. Read the requirements
and installation below to install everything correctly. This project has been tested on Kinect v2 it may
have issues with v1.

## Requirements

* Kinect
* Windows 8, 8.1 and Windows 10 (need fix)
* USB 3.0 port
* Visual Studio **2013**


## How To


Install Kinect v2 SDK from the official microsoft website: https://www.microsoft.com/en-us/download/details.aspx?id=44561

Then you must install Visual Studio 2013 as there are compatibility issues with versions >2013: https://my.visualstudio.com/Downloads?q=visual%20studio%202013&wt.mc_id=o~msft~vscom~older-downloads 

Once you installed the SDK and Visual Studio 2013 you can open the solution *Kinect2Sample2.sln*. The installation
depends on wether you are on Windows 8 or Windows 10. In both versions you have to set the CPU arch. before launching the project (see below):

![Select CPU](select_CPU.png)


### Windows 8, 8.1

The Kinect is supposed to work well on these Windows versions. If you have a problem to use it,
create an issue on gitlab. 

### Windows 10

On this version, you have to install some updates in order to make it work. Thanks to *brythi (Microsoft Employee)* who has
explained these four steps below (https://social.msdn.microsoft.com/Forums/en-US/82fa02b6-ef1e-4fb5-9821-87c4fea99fdb/update-to-address-win-81-apps-on-win-10-1809?forum=kinectv2sdk
): 

1. Make sure you are on Windows 10 1809+ (x64)
2. Install the updated driver + runtime either via windows update (PCs opted into Windows Insider fast/slow/release preview rings) or by downloading directly at: https://aka.ms/kinectdriver

If you download from the link above, please make sure you install by right clicking on kinectsensor.inf and selecting “install” (Do *not* run KinectRuntime-x64.msi directly).

3. Download the updated platform extension here:  https://aka.ms/kinect-platform-ext
4. To install the platform extension, extract the .zip file and double click on each of the .appx packages.  Make sure you install both packages.
After both updates are installed, you will need to ***grant both webcam and microphone permissions to a Win 8.1 app before it can access the Kinect***.
If you can't install .appx packages with *double-click* you can also use the following command in PowerShell:

`Add-AppxPackage -Path "C:\Path\to\File.Appx"`

You have now installed everything you need to launch the project. Now open the solution in Visual Studio 
and lauch the program. You should see the kinect light up and show something in the program. 




